<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't(nbNJ?nXaw<yTSN*!~H*M5vx1kZfc*biQ}dn4_o8(%OB,>zo?A]ZI2(wmKnEb/@' );
define( 'SECURE_AUTH_KEY',  '1c]GVTF+<$HiIPbgWt%&4jtZ~|ISJI;ucaUE7](<I?Nz|RI.&*#cj6lXS]J(U)KR' );
define( 'LOGGED_IN_KEY',    'oq]u8*:V1/?!gwx%WJHkZ!^%Yc<%J>1(H--(Cz?c.a@?7k.j 2u(XqW8E^RY.AzY' );
define( 'NONCE_KEY',        '(_(;/^TXlqp1DU<`mk^s;eg+(e2Y;V$_^P8h~W@HbPQat%s><qB0SOvw0kqZ4vT]' );
define( 'AUTH_SALT',        'eW MR-?Zwz^ERC~d&-<7%|9V}5e(5,AL@QtY!z {c$A5~=~ZKaa[XWpO?`lLZIJX' );
define( 'SECURE_AUTH_SALT', '@H; CAs|o)<0fuQr<B(e|/8.ve+6O{L_J{ez2Kyb=>#X3ed[Yso(;8u9v YsB`>Z' );
define( 'LOGGED_IN_SALT',   'OFly}yhF(Xz_g=5+m#igM<!=  ^=x<FpA {`wLvG]jKe@q3yPF~Xu_@/tW2 6$4K' );
define( 'NONCE_SALT',       '+x5tuPh3&75SVy:Ow}qwKAgkXL^@a}It!Zr[d]6f<eQH#wl+t.npWWIxFNOdqV{<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
