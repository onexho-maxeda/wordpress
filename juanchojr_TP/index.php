<?php include('components/_header.php'); ?>
<?php include('components/_navbar.php'); ?>

<section id="home">
	<div class="home-text">
		<p>Welcome To Our Studio!</p>
		<h1>It's Nice To Meet You</h1>
	<a class="button" href="">TELL ME MORE</a>
	</div>
</section>
<section id="services">
	<div class="services-header">
		<p class="title">SERVICES</p>
		<p class="sub-title">Proin iaculis purus consequat sem cure. </p>
	</div>
	<div class="container">
	<div class="service e-commerce">
		<img src="assets/images/Icon-Basket.png" alt="Icon-Basket.jpg">
		<div class="body">
			<h4 class="title">E-Commerce</h4>
			<p class="text">Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
		</div>
	</div>
	<div class="service responsive-web">
		<img src="assets/images/Icon-Laptop.png" alt="Icon-Basket.jpg">
		<div class="card-body">
			<h4 class="title">Responsive Web</h4>
			<p class="text">Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
		</div>
	</div>
	<div class="service web-security">
		<img src="assets/images/Icon-Locked.png" alt="Icon-Basket.jpg">
		<div class="card-body">
			<h4 class="title">Web Security</h4>
			<p class="text">Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
		</div>
	</div>
	</div>
</section>
<section id="portfolio">
	<div class="portfolio-header" data-aos="fade-up">
		<p class="title">OUR PORTFOLIO</p>
		<p class="sub-title">Proin iaculis purus consequat sem cure. </p>
	</div>
	<div class="container">
		<div class="portfolios">
		<div class="portfolio-thumbnail">
	      <img src="assets/images/portfolio-row1-col1.jpg">
	      <div class="caption">
	        <p class="title">Ebony & Ivory</p>
	        <p class="description">Branding</p>
	      </div>
	    </div>
	    <div class="portfolio-thumbnail">
	      <img src="assets/images/portfolio-row1-col2.jpg">
	      <div class="caption">
	        <p class="title">Smart Stationary</p>
	        <p class="description">Print Design</p>
	      </div>
	    </div>
	    <div class="portfolio-thumbnail">
	      <img src="assets/images/portfolio-row1-col3.jpg">
	      <div class="caption">
	        <p class="title">Clever Poster</p>
	        <p class="description">Print Design</p>
	      </div>
	    </div>
	    <div class="portfolio-thumbnail	">
	      <img src="assets/images/portfolio-row2-col1.jpg">
	      <div class="caption">
	        <p class="title">Vinyl Record</p>
	        <p class="description">Product Mock-up</p>
	      </div>
	    </div>
	    <div class="portfolio-thumbnail">
	      <img src="assets/images/portfolio-row2-col2.jpg">
	      <div class="caption">
	        <p class="title">Treehouse Template</p>
	        <p class="description">Web Design</p>
	      </div>
	    </div>
	    <div class="portfolio-thumbnail">
	      <img src="assets/images/portfolio-row2-col3.jpg">
	      <div class="caption">
	        <p class="title">Burned Logo</p>
	        <p class="description">Branding</p>
	      </div>
	    </div>
	</div>
	</div>
</section>
<section id="about-us">
	<div class="header" data-aos="fade-up">
		<p class="title">ABOUT US</p>
		<p class="sub-title">Proin iaculis purus consequat sem cure. </p>
	</div>
<div class="container">
	<div class="timeline">
		<div class="vertical"></div>
		<ul>
		<li>
			<div class="content">
				<img src="assets/images/Image-Our Humble Beginnings.jpg">
				<h3>JULY 2010<span>Our Humble Beginnings</span></h3>
				<p>Proin iaculis purus consequat sem cure 
				digni ssim. Donec porttitora entum suscipit 
				aenean rhoncus posuere odio in tincidunt. Proin 
				iaculis purus consequat sem cure digni 
				ssim. Donec porttitora entum suscipit.</p>
			</div>
		</li>
		<li>
			<div class="content">
				<img src="assets/images/Image-Facing Startup Battles.jpg">
				<h3>JANUARY 2012 <span>Facing Startup Battles</span></h3>
				<p>Proin iaculis purus consequat sem cure 
				digni ssim. Donec porttitora entum suscipit 
				aenean rhoncus posuere odio in tincidunt. Proin 
				iaculis purus consequat sem cure digni 
				ssim. Donec porttitora entum suscipit.</p>
			</div>
		</li>
		<li>
			<div class="content">
				<img src="assets/images/Image-Enter The Dark Days.jpg">
				<h3>DECEMBER 2012 <span>Enter The Dark Days</span></h3>
				<p>Proin iaculis purus consequat sem cure 
				digni ssim. Donec porttitora entum suscipit 
				aenean rhoncus posuere odio in tincidunt. Proin 
				iaculis purus consequat sem cure digni 
				ssim. Donec porttitora entum suscipit.</p>
			</div>
		</li>
		<li>
			<div class="content">
				<img src="assets/images/Image-Our Triumph.jpg">
				<h3>FEBRUARY 2014 <span> Our Triumph</h3>
				<p>Proin iaculis purus consequat sem cure 
				digni ssim. Donec porttitora entum suscipit 
				aenean rhoncus posuere odio in tincidunt. Proin 
				iaculis purus consequat sem cure digni 
				ssim. Donec porttitora entum suscipit.</p>
			</div>
		</li>
		<li>
			<div class="content">
				<h3>OUR <br>STORY <br> CONTINUES <br>...</h3>
			</div>
		</li>
		</ul>
	</div>
</div>
</section>
<section id="our-amazing-team">
	<div class="container">
	<div class="header" data-aos="fade-up">
		<p class="title">OUR AMAZING TEAM</p>
		<p class="sub-title">Proin iaculis purus consequat sem cure. </p>
	</div>
	<div class="team">
			<div class="member">
				<img src="assets/images/Member.jpg">
				<h4>Juancho Maceda</h4>
				<p>Position</p>
				<div class="social">
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-facebook-f"></i></a>
					<a href=""><i class="fa fa-google-plus"></i></a>
				</div>
			</div>
	</div>
	<div class="description">
		<p>Proin iaculis purus consequat sem cure  digni ssim donec porttitora entum suscipit  
		aenean rhoncus posuere odio in tincidunt proin iaculis.</p>
	</div>
	</div>
</section>
<section id="logos">
	<div class="container">
		<div class="logo-image">
			<img src="assets/images/Logos-Envato.jpg">
			<img src="assets/images/Logos-WordPress.png">
			<img src="assets/images/Logos-Tuts+.jpg">
			<img src="assets/images/Logos-Microlancer.jpg">
		</div>
	</div>
</section>
<section id="contact-us">
	<div class="header" data-aos="fade-up">
		<p class="title">CONTACT US</p>
		<p class="sub-title">Proin iaculis purus consequat sem cure. </p>
	</div>

	<form>
		<div>
			<input type="text" class="form-control" name="first_name" placeholder="YOUR NAME *">
	    	<input type="text" class="form-control" name="first_name" placeholder="YOUR EMAIL *">
	    	<input type="text" class="form-control" name="first_name" placeholder="SUBJECT *">
		</div>
		<div>
			<textarea class="form-control" placeholder="YOUR MESSAGE *"></textarea>
		</div>
    	<input class="button" type="submit" name="" value="SEND MESSAGE">
	</form>
</section>
<?php include('components/_footer.php'); ?>
