<?php $body_class = 'page-generic'; ?>
<?php include('components/_header.php'); ?>
<?php include('components/_navbar.php'); ?>

<section id="contact-page">
	<div class="container">
		<div class="title">
			<h1>CONTACT US</h1>
			<p>Proin iaculis purus consequat sem cure.</p>
		</div>
	</div>
	<div class="contact">
		<div class="details">
			<h2><img src="assets/images/gdpr_profile-picture.jpg">ME</h2>
			<p>	Proin iaculis purus consequat sem cure 
				digni ssim. Donec porttitora entum suscipit 
				aenean rhoncus posuere odio in tincidunt. Proin 
				iaculis purus consequat sem cure digni 
				ssim. Donec porttitora entum suscipit.</p>
		</div>
		<div class="contact-form">
			<p>Get in Touch</p>
			<input type="text" name="" placeholder="Name">
			<input type="text" name="" placeholder="Email">
			<textarea placeholder="Message"></textarea>
			<p>You can also call us at 09** *** **** or email <span>email@mail.com</span></p>
			<input type="button" class="" name="" value="SEND">
		</div>
	</div>
</section>
<section id="more-contact-details">
	<div>
		<div>
			<h5>About Us</h5>
			<p>Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit.</p>
			<a href="">Learn More</a>
		</div>
		<div>
			<h5>Location</h5>
			<p>Street Place City</p>
			<p>(012) 345-6789</p>
		</div>
		<div>
			<h5>Connect</h5>
			<p>Follow us on our social media</p>
			<i class="fa fa-facebook-f"></i>
			<i class="fa fa-twitter"></i>
			<i class="fa fa-google-plus"></i>
		</div>
	</div>
</section>

<?php include('components/_footer.php'); ?>
