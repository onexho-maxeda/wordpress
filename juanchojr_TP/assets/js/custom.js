$(document).ready(function() {


mobile_menu();
header();

function mobile_menu() {

	$('#burger-menu').click(function() {
		$('.nav-menu-mobile').animate({"right":"0px"}, 200);
	});

	$('.menu-close').click(function(e) {
		e.preventDefault();
		$('.nav-menu-mobile').animate({"right":"-320px"}, 200);//.css('right', '-320px');
	});

}


function header(){

	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if(scroll > 100){
			$("#header").css({
				"background":"#fed136",
				"padding":"25px 125px"
			});

			$("#logo p").css("color","#222222");
		}
		else {
			$("#logo p").css("color","#fed136");
			$("#header").css({
				"background":"transparent",
				"padding":"43px 125px"
			});
		}
	});

}

});