<section id="footer">
	<div class="container">
		<footer>
			<div class="copyright">
				<p>&copy Copyright 2014 FreebiesXpress.com</p>
			</div>
			<div class="social">
				<a href=""><i class="fa fa-facebook-f"></i></a>
				<a href=""><i class="fa fa-twitter"></i></a>
				<a href=""><i class="fa fa-google-plus"></i></a>
				<a href=""><i class="fa fa-pinterest-p"></i></a>
			</div>
	</footer>
	</div>
</section>

<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/aos/aos.js"></script>
<script type="text/javascript">
	AOS.init();
</script>

</body>
</html>