	<div id="header">
	<div id="logo"><p>Golden</p></div>
	<nav id="nav-menu-container">
		<ul class="nav-menu">
			<li class="menu"><a href="">Home</a></li>
			<li class="menu"><a href="">Services</a></li>
			<li class="menu"><a href="">Portfolio</a></li>
			<li class="menu"><a href="">About</a></li>
			<li class="menu"><a href="">Contact</a></li>
		</ul>
		<div class="mobile-menu">
			<!-- Collapse button -->
			  <button id="burger-menu" class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
			    aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text">
			    	<i class="fa fa-bars fa-1x"></i></span>
			</button>
			<div class="nav-menu-mobile">
				<div class="nav-menu-mobile-inner">
					<a class="menu-close" href="#"><i class="fa fa-close"></i></button>
					<ul class="nav-menu">
						<li class="menu"><a href="">Home</a></li>
						<li class="menu"><a href="">Services</a></li>
						<li class="menu"><a href="">Portfolio</a></li>
						<li class="menu"><a href="">About</a></li>
						<li class="menu"><a href="">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	</div>