<!DOCTYPE html>
<html>
<head>
	<title>Golden</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/index.css" />
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap/bootstrap.min.css">
	<!-- Font Awesome -->
  	<link rel="stylesheet" href="assets/css/font-awesome/css/font-awesome.min.css">
  	<link rel="stylesheet" href="assets/aos/aos.css">
  	<!-- JQuery -->
  	<script type="text/javascript" src="assets/js/jquery.js"></script>
  
</head>
<body class="<?=!empty($body_class) ? $body_class : false;?>">