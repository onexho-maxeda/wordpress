<?php $body_class = 'page-generic'; ?>
<?php include('components/_header.php'); ?>
<?php include('components/_navbar.php'); ?>

<section id="content">
	<div class="container">
		<div class="title">
			<h1>[ PAGE TITLE ]</h1>
			<p>Proin iaculis purus consequat sem cure.</p>
		</div>
	</div>
</section>

<?php include('components/_footer.php'); ?>
