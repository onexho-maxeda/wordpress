<?php get_header(); ?>

    <?php 
    if ( have_posts() ) { ?>
            <?php
            // Start the loop.
            while ( have_posts() ) {
                the_post();
                ?><h1><?= get_the_title(); ?></h1><?php
                ?><div><?= get_the_content(); ?></div><?php
            }

            // If no content, include the "No posts found" template.
        }
    ?>

<?php get_footer(); ?>
