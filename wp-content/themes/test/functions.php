<?php

function template_assets(){
    wp_enqueue_style( 'template-style', get_stylesheet_directory_uri()."/assets/style.css");
}

add_action( 'wp_enqueue_scripts', 'template_assets' );